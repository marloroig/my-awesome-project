### Procedimiento que he realizado para poder crear mi repositorio remoto es el siguiente
___

- Lo primero que hacemos al instalar Git es establecer nuestro nombre de usuario y dirección de correo electrónico conlos comandos:

    $ git config --global user.name "Marlo Roig Calero"

    $ git config --global user.email "marloroig@estudiante.edib.es"

- indicamos que acceda a nuestro disco local que deemos en mi caso el D, con el siguiente comando :

    $ cd D: 

- Creamos la carpeta repositorio en la cual guardaremos nuestros archivos con el comando :

    $ mkdir repositorio

- Indicamos que acceda a la carpeta repositorio con el comando:

    $ cd repositorio

- Creamos el archivo index.html con el comando :

    $ touch index.html

- Añadimos el archivo al área de ensayo con el comando:

    $ git add index.html

- Comprovamos el estado del repositorio con el comando :

    $ git status

- Con el siguinete comando ($ nano index.html) modificaremos el archivo index.html que hemos creado y crearemos una pequeña interfaz , una vez acabemo (ctrl+x) para salir y guardar.

- Si revisamos el estatus ahora después de modificar el index.html nos saldrá como que no se han preparado para ser comitados, por eso los tenemos que volver a añadir de nuevo con el comando 

    $ git add index.html

- creamos el archivo index.js con el comando:

    $ touch index.js

- Introduciremos la informacion que deseemos en el archivo con el comando:

    $ nano index.js

- Añadiremos el archivo index.js al area de ensayo con el comando:

    $ git add index.js

- Comprobamos que todo haya ido bien con el comando:

    $ git status

- Creamos el archivo README.MD con el comando:

    $ touch README.MD


- Modificamos el archivo README.MD con el siguiente comando:

    $ nano README.MD

- Añadiremos el archivo index.js al área de ensayo con el comando:

    $ git add README.MD

- Comprobamos que todo haya ido bien con el comando:

    $ git status

- Hacemos el primer comit de prueba con el comando:

    $ git commit -m "Primer commit de prueba"

- Hacemos uns comprobación después del commit para ver si todo ha ido bien con el comando:

    $ git log

- Decidimos modificar el archivo README.MD con el comando :

    $ nano README.MD

- Hacemos un git status para ver como estan los archivos y nos encontramos con esto:

- Para que todo vaya bien debemos realizar un :

    $ git add README.MD

- Revisar que el git add haya ido bien con el comando:

    $ git status

- Hacemos un commit para poder devolver el archivo README.MD al repositorio local con el comando :

    $ git commit -m "segundo commit de readme, para verificar"

- Realizamos un git log otra vez para verificar que el commit ha sido realizado con éxito:

    $ git log

- Enlazamos el repositorio que hemos creado con nuestro repositorio remoto de gitlab: 

    $ git remote add origin https://gitlab.com/-/ide/project/marloroig/my-awesome-project.git

- Realizamos el git push para poder subir nuestra carpeta de repositorio a nuestro perfil de git lab con el comando : 
    $ git push -u origin main

